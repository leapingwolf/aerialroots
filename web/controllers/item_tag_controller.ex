defmodule Aerialroots.ItemTagController do
  use Aerialroots.Web, :controller

  alias Aerialroots.ItemTag

  plug :scrub_params, "item_tag" when action in [:create, :update]

  def index(conn, _params) do
    itemtag = Repo.all(ItemTag)
    render(conn, "index.json", itemtag: itemtag)
  end

  def create(conn, %{"item_id" => item_id, "tag_id" => tag_id}) do
    IO.inspect("m i here in creatte")
  end 

  def create(conn, %{"item_tag" => item_tag_params}) do
    
    changeset = ItemTag.changeset(%ItemTag{}, item_tag_params)

    case Repo.insert(changeset) do
      {:ok, item_tag} ->
        conn
        |> put_status(:created)
        |> put_resp_header("location", item_tag_path(conn, :show, item_tag))
        |> render("show.json", item_tag: item_tag)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Aerialroots.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    item_tag = Repo.get!(ItemTag, id)
    render(conn, "show.json", item_tag: item_tag)
  end

  def update(conn, %{"id" => id, "item_tag" => item_tag_params}) do
    item_tag = Repo.get!(ItemTag, id)
    changeset = ItemTag.changeset(item_tag, item_tag_params)

    case Repo.update(changeset) do
      {:ok, item_tag} ->
        render(conn, "show.json", item_tag: item_tag)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Aerialroots.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    item_tag = Repo.get!(ItemTag, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(item_tag)

    send_resp(conn, :no_content, "")
  end
end
