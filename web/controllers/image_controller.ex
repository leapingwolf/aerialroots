defmodule Aerialroots.ImageController do
  use Aerialroots.Web, :controller
  require Logger
  import Ecto.Query
  alias Aerialroots.Image
  alias Aerialroots.ImageController

  plug :scrub_params, "image" when action in [:create, :update]
  plug Guardian.Plug.EnsureAuthenticated, handler: __MODULE__

 def uploadImage(conn, %{"item_id" => item_id, "picture" => imgRawdata, "image" => image_params}) do 
  IO.inspect("M In upload image")
    parsedImageParams = Poison.Parser.parse!(image_params)
    # parsedImageId = Poison.Parser.parse!(id)
    IO.inspect(parsedImageParams)
    current_item = Map.put(parsedImageParams["image"], "id", item_id) 
    task = Task.async(Image, :store, [{imgRawdata.path, current_item}])
    return =  Task.await(task)
    case return do
       {:ok, path} ->
          sequence = Map.get(parsedImageParams["image"], "sequence")
          url = Image.url({imgRawdata.filename, current_item})
          image_params = Map.put(parsedImageParams["image"], "url", url)
          image_params = Map.put(image_params, "item_id", item_id)
          image_params = Map.put(image_params, "uploadStatus", true)
          # IO.inspect(image_params)
          #if image for the item id and sequence exists, replace it
          existingImageRecord = Image 
                    |> where([image], image.sequence == ^sequence and image.item_id == ^item_id)
                    |> Aerialroots.Repo.all()

           case Enum.count(existingImageRecord) do
              0 -> 
                ImageController.create(conn, %{"image" => image_params})  
              1 ->  
                imageId = Enum.at(existingImageRecord,0).id
                IO.inspect(imageId)
                IO.inspect(image_params)
                # image_params = Map.put(image_params, "id", imageId) 
                ImageController.update(conn, %{"id" => imageId, "image" => image_params}) 
            end

        {:error, :invalid_file} ->         
          conn
          |> put_status(:error)
          |> render(Aerialroots.ChangesetView, "error.json", upload: :error)
        {:error, :timeout} ->  
          IO.inspect("here") 
          send_resp(conn, :timeout, "connection time out")
        {:error, error} -> 
          IO.inspect("here11")        
          conn
              |> put_status(:error)
              |> render(Aerialroots.ChangesetView, "error.json", upload: error)
    end   
  end  

  def index(conn, _params) do
    image = Repo.all(Image)
    render(conn, "index.json", image: image)
  end

  def create(conn, %{"image" => image_params}) do
    changeset = Image.changeset(%Image{}, image_params)

    case Repo.insert(changeset) do
      {:ok, image} ->
        conn
        |> put_status(:created)
        |> put_resp_header("location", image_path(conn, :show, image))
        |> render(Aerialroots.ImageView, "show.json", image: image)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Aerialroots.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    image = Repo.get!(Image, id)
    render(conn, "show.json", image: image)
  end

  def update(conn, %{"id" => id, "image" => image_params}) do
    image = Repo.get!(Image, id)
    changeset = Image.changeset(image, image_params)

    case Repo.update(changeset) do
      {:ok, image} ->
        conn
        |> render(Aerialroots.ImageView, "show.json", image: image)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Aerialroots.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    image = Repo.get!(Image, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(image)

    send_resp(conn, :no_content, "")
  end

   def unauthenticated(conn, _params) do
    conn
    |> put_flash(:error, "Authentication required")
    #|> redirect(to: login_path(conn, :create, :login))
    Logger.info("unauthenticated")
    send_resp(conn, :no_content ,"auth required")
  end
end
