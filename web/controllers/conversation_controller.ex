  defmodule Aerialroots.ConversationController do
  use Aerialroots.Web, :controller
  require Logger
  import Ecto.Query
  alias Aerialroots.Message
  alias Aerialroots.Conversation
  alias Aerialroots.User
  alias Aerialroots.Item
  alias Aerialroots.Image

  plug :scrub_params, "conversation" when action in [:create, :update]
  plug Guardian.Plug.EnsureAuthenticated, handler: __MODULE__

  def test(conn) do
    send_resp(conn, :no_content, "Conversation page is not accessible without auth")
  end

  def index(conn, %{"page" => page}) do
    #all conversations where current user is itemOwner or Enquirer
    user = Guardian.Plug.current_resource(conn)
    IO.inspect("m i here")
    #get all conversations with corresponding message trail in each - Messages tab on Home screen
    conversation = Conversation 
                   |> preload(:message) 
                   |> join(:inner, [conversation], sender in User,conversation.sender_id == sender.id) 
                   |> join(:inner, [conversation], receiver in User,conversation.receiver_id == receiver.id) 
                   |> join(:inner, [conversation], item in Item, conversation.item_id == item.id) 
                   |> join(:left, [_,_,_,item], image in Image, item.id == image.item_id) 
                   |> where([conversation,_,_,_,image], (conversation.sender_id == ^user.id or conversation.receiver_id == ^user.id) and (image.sequence == 1 or is_nil(image.sequence))) 
                   |> select([conversation, sender, receiver, item, image], %{ "conversation": conversation,  "receiver": receiver, "sender": sender, "item": item, "image": image})
                   |> order_by([conversation,_,_,_,_], [desc: conversation.updated_at]) 
                   |> Repo.paginate(page: page)
    render(conn, "conversationUserDetail.json", conversationUserDetail: conversation.entries, page_number: conversation.page_number,
    page_size: conversation.page_size,
    total_pages: conversation.total_pages,
    total_entries: conversation.total_entries)
  end


  # def index(conn, %{"page" => page, "item_id" => item_id}) do
  #   message = Conversation 
  #   |> join(:left, [conversation], sender in User,sender.id == conversation.sender_id) 
  #   |> join(:left, [conversation], receiver in User,receiver.id == conversation.receiver_id) 
  #   |> join(:left, [conversation], message in Message,message.conversation_id == conversation.id) 
  #   |> where([conversation], conversation.item_id == ^item_id)
  #   render(conn, "index.json", message: message)
  # end


  def create(conn, %{"conversation" => conversation_params}) do
    Logger.info("conversation create")
    changeset = Conversation.changeset(%Conversation{}, conversation_params)

    case Repo.insert(changeset) do
      {:ok, conversation} ->
        conn
        |> put_status(:created)
        #|> put_resp_header("location", conversation_path(conn, :show, conversation))
        |> render("show.json", conversation: conversation)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Aerialroots.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def index(conn, %{"sender_id" => sender_id, "receiver_id" => receiver_id, "item_id" => item_id}) do
    #load all messages where between the 2 users of the conversation - Item Detail screen -> Message click
   conversation = Conversation 
    |> join(:left, [conversation], sender in User,sender.id == ^sender_id) 
    |> join(:left, [conversation], receiver in User,receiver.id == ^receiver_id) 
    |> where([c], 
      (c.sender_id == ^sender_id or c.receiver_id == ^sender_id) 
      and (c.receiver_id == ^receiver_id or c.sender_id == ^receiver_id) 
      and c.item_id == ^item_id)
    |> Aerialroots.Repo.one()

    if(is_nil(conversation)) do
      message = Message
      |> join(:left, [message], sender in User,sender.id == message.sender_id) 
      |> join(:left, [message], receiver in User,receiver.id == message.receiver_id) 
      |> where([message], is_nil(message.conversation_id))
      |> select([message,sender,receiver], %{"message": message, "sender": sender.name, "receiver": receiver.name})
      |> order_by([message], message.sequence)
      |> Repo.all()      
    else  
      message = Message
      |> join(:left, [message], sender in User,sender.id == message.sender_id) 
      |> join(:left, [message], receiver in User,receiver.id == message.receiver_id) 
      |> where([message],message.conversation_id == ^conversation.id)      
      |> select([message,sender,receiver], %{"message": message, "sender": sender.name, "receiver": receiver.name})
      |> order_by([message], message.sequence)
      |> Repo.all()
    end      
      conn 
      |> render(Aerialroots.MessageView, "showAll.json", message: message)

  end

  def show(conn, %{"id" => id}) do
    # conversation = Repo.get!(Conversation, id) |> Repo.preload(:message)
      message = Message
      |> join(:left, [message], sender in User,sender.id == message.sender_id) 
      |> join(:left, [message], receiver in User,receiver.id == message.receiver_id) 
      |> where([message],message.conversation_id == ^id)      
      |> select([message,sender,receiver], %{"message": message, "sender": sender.name, "receiver": receiver.name})
      |> order_by([message], message.sequence)
      |> Repo.all()
      
      conn 
      |> render(Aerialroots.MessageView, "showAll.json", message: message)
  end

  def upsert(conn, %{"conversation" => conversation_params}) do
    #insert a message in a new conversation or to an existing conversation for a specific item
    message = Map.get(conversation_params, "message") |> Enum.at(0)
    sender_id = Map.get(message, "sender_id")
    item_id = Map.get(conversation_params, "item_id")
    receiver_id = Map.get(message, "receiver_id")

    #message = Map.get(conversation_params, "message")
    
    existingConversation = Conversation 
    |> preload(:message)
    |> where([c], (c.sender_id == ^sender_id or c.sender_id == ^receiver_id)         
        and (c.receiver_id == ^sender_id or c.receiver_id == ^receiver_id) 
        and c.item_id == ^item_id)
    |> Aerialroots.Repo.all()

     case Enum.count(existingConversation) do
       0 ->
        {:ok, messageList} =  Repo.transaction fn ->
          conversation_params = %Aerialroots.Conversation{sender_id: sender_id, receiver_id: receiver_id, item_id: item_id}
          newConversation = Aerialroots.Repo.insert!(conversation_params)  
          newMessage = %Aerialroots.Message{body: Map.get(message,"body") , sender_id: sender_id, receiver_id: receiver_id, sequence: 1, conversation_id: newConversation.id}
          # changeset = Message.changeset{%Message{}, newMessage}
          Repo.insert!(newMessage)
              
          messageList  = Message
          |> join(:inner, [message], sender in User,sender.id == message.sender_id) 
          |> join(:inner, [message], receiver in User,receiver.id == message.receiver_id)
          |> where([message],message.conversation_id == ^(newConversation.id))  
          |> select([message,sender,receiver], %{"message": message, "sender": sender.name, "receiver": receiver.name})    
          |> order_by([message], message.sequence)
          |> Repo.all()
          
        end

        conn 
        |> render(Aerialroots.MessageView, "showAll.json", message: messageList)                  

       1 ->  
        #get current message seq for the conversation

        maxSequence = Message |> where([m], m.conversation_id == ^Enum.at(existingConversation,0).id) |> select([r], max(r.sequence)) |> Repo.one()
        message_params = [%{body: Map.get(message,"body") , sender_id: sender_id, receiver_id: receiver_id, sequence: maxSequence + 1}]
        associatedMessage = Ecto.build_assoc(Enum.at(existingConversation,0), :message, Enum.at(message_params,0))

        Repo.insert!(associatedMessage)

        messageList = Message
        |> join(:left, [message], sender in User,sender.id == message.sender_id) 
        |> join(:left, [message], receiver in User,receiver.id == message.receiver_id) 
        |> where([message],message.conversation_id == ^Enum.at(existingConversation,0).id)      
        |> select([message,sender,receiver], %{"message": message, "sender": sender.name, "receiver": receiver.name})
        |> order_by([message], message.sequence)
        |> Repo.all()

         conn 
         |> render(Aerialroots.MessageView, "showAll.json", message: messageList)

       _ -> 
        #error state : render empty message array

        messageList  = Message
        |> join(:inner, [message], sender in User,sender.id == message.sender_id) 
        |> join(:inner, [message], receiver in User,receiver.id == message.receiver_id)
        |> where([message],is_nil(message.conversation_id))  
        |> select([message,sender,receiver], %{"message": message, "sender": sender.name, "receiver": receiver.name})    
        |> order_by([message], message.sequence)
        |> Repo.all()
      

        conn 
        |> render(Aerialroots.MessageView, "showAll.json", message: messageList)                  

    end
  end


  def add_message(conn, %{"conversation_id" => conversation_id, "message" => message_params}) do
    Logger.info("in add_message")
    maxSequence = Message 
                  |> where([m], m.conversation_id == ^conversation_id )
                  |> select([r], max(r.sequence)) 
                  |> Repo.one()
    #conversation = Conversation |> Repo.get(conversation_id) |> Repo.preload([:message])  
    message_params = Map.put(message_params, "conversation_id", conversation_id)
    message_params = Map.put(message_params, "sequence", maxSequence + 1)
    IO.inspect(message_params)
    changeset = Message.changeset(%Message{}, message_params)
    Repo.insert!(changeset)
    send_resp(conn, 200, "Message inserted successfully")
  end


  def get_message(conn, %{"conversation_id" => conversation_id}) do
    conversation = Conversation |> Repo.get(conversation_id) |> Repo.preload([:message])  
    render(conn, "show.json", conversation: conversation)
  end

  defp update(conn, %{"id" => id, "conversation" => conversation_params}) do
    conversation = Repo.get!(Conversation, id) |> Repo.preload(:message)
    changeset = Conversation.changeset(conversation, conversation_params)
    #only allow updating the date time when a new message is added for the conversation
    #changeset = Conversation.changeset(%Conversation{},, "updated_at", Map.get(conversation_params, "updated_at"))

    case Repo.update(changeset) do
      {:ok, conversation} ->
        render(conn, "show.json", conversation: conversation)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Aerialroots.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    conversation = Repo.get!(Conversation, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(conversation)

    send_resp(conn, :no_content, "")
  end

  def unauthenticated(conn, _params) do
    conn
    |> put_flash(:error, "Authentication required")
    #|> redirect(to: login_path(conn, :create, :login))
    Logger.info("unauthenticated")
    send_resp(conn, :no_content ,"auth required")
  end
end
