defmodule Aerialroots.ItemController do
  use Aerialroots.Web, :controller
  require Logger
  import Ecto.Query
  alias Aerialroots.Item
  alias Aerialroots.User
  alias Aerialroots.ItemTag
  alias Aerialroots.Image
  alias Aerialroots.ImageController
  alias Aerialroots.Conversation
  alias Aerialroots.Message

  plug Guardian.Plug.EnsureAuthenticated, handler: __MODULE__

  plug :scrub_params, "item" when action in [:create, :update]

  def index(conn, %{"where" => where, "latitude" => latitude, "longitude" => longitude, "page" => page, "orderBy" => orderBy}) do
    Logger.info("m i here in where with latitude")
    longitude = String.to_float(longitude)
    latitude = String.to_float(latitude)
    image_preload_query = from i in Image, order_by: i.sequence
    case orderBy do 
      "0" ->
            Logger.info("m i where 0")
            item = Item 
            |> join(:inner, [item], user in User, item.user_id == user.id) 
            |> preload([:itemtag, itemtag: :tag]) |> preload(image: ^image_preload_query)
            |> join(:left, [item, user], conversation in Conversation, 
              (conversation.sender_id == user.id or conversation.receiver_id == user.id) 
              and conversation.item_id == item.id) 
            |> where([item,user], like(item.title, ^("%#{where}%")))
            |> select([item, user, conversation],
              %{  "item": item,  
                  "user": user, 
                  "conversationId": conversation.id,
                  "distance": fragment("ST_Distance_Sphere((ST_MakePoint(?,?)), (ST_MakePoint(?, ?)))", item.longitude, item.latitude, ^longitude, ^latitude) 
                })
            |> order_by([item,_], [desc: item.inserted_at]) 
            |> Repo.paginate(page: page) 

      "1" ->  
            Logger.info("m i where 1")
            item = Item 
            |> join(:inner, [item], user in User, item.user_id == user.id) 
            |> preload([:itemtag, itemtag: :tag]) |> preload(image: ^image_preload_query)
            |> join(:left, [item, user], conversation in Conversation, 
              (conversation.sender_id == user.id or conversation.receiver_id == user.id) 
              and conversation.item_id == item.id)             
            |> where([item,user], like(item.title, ^("%#{where}%")))
            |> select([item, user, conversation], 
              %{  "item": item,  
                  "user": user, 
                  "conversationId": conversation.id,
                  "distance": fragment("ST_Distance_Sphere((ST_MakePoint(?,?)), (ST_MakePoint(?, ?)))", item.longitude, item.latitude, ^longitude, ^latitude) 
                })
            |> order_by([item,_], [desc: item.updated_at])
            |> order_by([item,_], [desc: item.updated_at]) 
            |> Repo.paginate(page: page) 

            IO.inspect(item)

       _ -> 
            item = Item 
            |> join(:inner, [item], user in User, item.user_id == user.id) 
            |> preload([:itemtag, itemtag: :tag]) |> preload(image: ^image_preload_query)
            |> join(:left, [item, user], conversation in Conversation, 
              (conversation.sender_id == user.id or conversation.receiver_id == user.id) 
              and conversation.item_id == item.id) 
            |> where([item,user], like(item.title, ^("%#{where}%")))
            |> select([item, user, conversation], 
              %{  "item": item,  
                  "user": user, 
                  "conversationId": conversation.id,
                  "distance": fragment("ST_Distance_Sphere((ST_MakePoint(?,?)), (ST_MakePoint(?, ?)))", item.longitude, item.latitude, ^longitude, ^latitude) 
                })
            |> order_by([item,_], [desc: item.updated_at]) 
            |> Repo.paginate(page: page) 
    end  
    render(conn, "index.json", item: item.entries, page_number: item.page_number,
    page_size: item.page_size,
    total_pages: item.total_pages,
    total_entries: item.total_entries)
  end

  def index(conn, %{"where" => where, "page" => page, "orderBy" => orderBy}) do
    Logger.info("m i here in where")
    image_preload_query = from i in Image, order_by: i.sequence
    case orderBy do 
      "0" ->
            Logger.info("m i where 0")
            item = Item 
            |> join(:inner, [item], user in User, item.user_id == user.id) 
            |> preload([:itemtag, itemtag: :tag]) |> preload(image: ^image_preload_query)
            |> join(:left, [item, user], conversation in Conversation, 
              (conversation.sender_id == user.id or conversation.receiver_id == user.id) 
              and conversation.item_id == item.id) 
            |> where([item,user], like(item.title, ^("%#{where}%")))
            |> select([item, user, conversation],
              %{  "item": item,  
                  "user": user, 
                  "conversationId": conversation.id,
                  "distance": nil 
                })
            |> order_by([item,_], [desc: item.inserted_at]) 
            |> Repo.paginate(page: page) 

      "1" ->  
            Logger.info("m i where 1")
            item = Item 
            |> join(:inner, [item], user in User, item.user_id == user.id) 
            |> preload([:itemtag, itemtag: :tag]) |> preload(image: ^image_preload_query)
            |> join(:left, [item, user], conversation in Conversation, 
              (conversation.sender_id == user.id or conversation.receiver_id == user.id) 
              and conversation.item_id == item.id)             
            |> where([item,user], like(item.title, ^("%#{where}%")))
            |> select([item, user, conversation], 
              %{  "item": item,  
                  "user": user, 
                  "conversationId": conversation.id,
                  "distance": nil
                })
            |> order_by([item,_], [desc: item.updated_at])
            |> order_by([item,_], [desc: item.updated_at]) 
            |> Repo.paginate(page: page) 

            IO.inspect(item)

       _ -> 
            item = Item 
            |> join(:inner, [item], user in User, item.user_id == user.id) 
            |> preload([:itemtag, itemtag: :tag]) |> preload(image: ^image_preload_query)
            |> join(:left, [item, user], conversation in Conversation, 
              (conversation.sender_id == user.id or conversation.receiver_id == user.id) 
              and conversation.item_id == item.id) 
            |> where([item,user], like(item.title, ^("%#{where}%")))
            |> select([item, user, conversation], 
              %{  "item": item,  
                  "user": user, 
                  "conversationId": conversation.id,
                  "distance": nil
                })
            |> order_by([item,_], [desc: item.updated_at]) 
            |> Repo.paginate(page: page) 
    end  
    render(conn, "index.json", item: item.entries, page_number: item.page_number,
    page_size: item.page_size,
    total_pages: item.total_pages,
    total_entries: item.total_entries)
  end
    
  def index(conn, %{"page" => page, "latitude" => latitude, "longitude" => longitude , "orderBy" => orderBy}) do
    longitude = String.to_float(longitude)
    latitude = String.to_float(latitude)
    image_preload_query = from i in Image, order_by: i.sequence
    Logger.info("getting current_resource")
    user = Guardian.Plug.current_resource(conn)    
    IO.inspect(user)
    case orderBy do 
      "0" ->
            Logger.info("m i here 0")
            item = Item 
            |> join(:inner, [item], user in User, item.user_id == user.id) 
            |> preload([:itemtag, itemtag: :tag]) |> preload(image: ^image_preload_query)
            |> join(:left, [item, user], conversation in Conversation, 
              (conversation.sender_id == user.id or conversation.receiver_id == user.id) 
              and conversation.item_id == item.id) 
            |> select([item, user, conversation], 
              %{  "item": item,  
                  "user": user, 
                  "conversationId": conversation.id,
                  "distance": fragment("ST_Distance_Sphere((ST_MakePoint(?,?)), (ST_MakePoint(?, ?)))", item.longitude, item.latitude, ^longitude, ^latitude) 
                })
            |> order_by([item,_], [desc: item.inserted_at]) 
            |> Repo.paginate(page: page) 

      "1" ->  
            Logger.info("m i here 1")
            item = Item 
            |> join(:inner, [item], user in User, item.user_id == user.id) 
            |> preload([:itemtag, itemtag: :tag]) |> preload(image: ^image_preload_query)
            |> join(:left, [item, user], conversation in Conversation, 
              (conversation.sender_id == user.id or conversation.receiver_id == user.id) 
              and conversation.item_id == item.id) 
            |> select([item, user, conversation], 
              %{  "item": item,  
                  "user": user, 
                  "conversationId": conversation.id,
                  "distance": fragment("ST_Distance_Sphere((ST_MakePoint(?,?)), (ST_MakePoint(?, ?)))", item.longitude, item.latitude, ^longitude, ^latitude) 
                })
            |> order_by([item,_], [desc: item.updated_at]) 
            |> Repo.paginate(page: page) 

       _ -> 
            item = Item 
            |> join(:inner, [item], user in User, item.user_id == user.id) 
            |> preload([:itemtag, itemtag: :tag]) |> preload(image: ^image_preload_query)
            |> join(:left, [item, user], conversation in Conversation, 
              (conversation.sender_id == user.id or conversation.receiver_id == user.id) 
              and conversation.item_id == item.id) 
            |> select([item, user, conversation], 
              %{  "item": item,  
                  "user": user, 
                  "conversationId": conversation.id,
                  "distance": fragment("ST_Distance_Sphere((ST_MakePoint(?,?)), (ST_MakePoint(?, ?)))", item.longitude, item.latitude, ^longitude, ^latitude) 
                })
            |> order_by([item,_], [desc: item.updated_at]) 
            |> Repo.paginate(page: page) 
    end  
    render(conn, "index.json", item: item.entries, page_number: item.page_number,
    page_size: item.page_size,
    total_pages: item.total_pages,
    total_entries: item.total_entries)
  end

  def index(conn, %{"page" => page, "orderBy" => orderBy}) do
    Logger.info("HERE HERE")
    user = Guardian.Plug.current_resource(conn)    
    image_preload_query = from i in Image, order_by: i.sequence
    IO.inspect(user)
    case orderBy do 
      "0" ->
            Logger.info("m i here 0")
            item = Item 
            |> join(:inner, [item], user in User, item.user_id == user.id) 
            |> preload([:itemtag, itemtag: :tag]) |> preload(image: ^image_preload_query)
            |> join(:left, [item, user], conversation in Conversation, 
              (conversation.sender_id == user.id or conversation.receiver_id == user.id) 
              and conversation.item_id == item.id) 
            |> select([item, user, conversation], 
              %{  "item": item,  
                  "user": user, 
                  "conversationId": conversation.id,
                  "distance": nil
                })
            |> order_by([item,_], [desc: item.inserted_at]) 
            |> Repo.paginate(page: page) 

      "1" ->  
            Logger.info("m i here 1")
            item = Item 
            |> join(:inner, [item], user in User, item.user_id == user.id) 
            |> preload([:itemtag, itemtag: :tag]) |> preload(image: ^image_preload_query)
            |> join(:left, [item, user], conversation in Conversation, 
              (conversation.sender_id == user.id or conversation.receiver_id == user.id) 
              and conversation.item_id == item.id) 
            |> select([item, user, conversation], 
              %{  "item": item,  
                  "user": user, 
                  "conversationId": conversation.id,
                  "distance": nil
                })
            |> order_by([item,_], [desc: item.updated_at]) 
            |> Repo.paginate(page: page) 

       _ -> 
            item = Item 
            |> join(:inner, [item], user in User, item.user_id == user.id) 
            |> preload([:itemtag, itemtag: :tag]) |> preload(image: ^image_preload_query)
            |> join(:left, [item, user], conversation in Conversation, 
              (conversation.sender_id == user.id or conversation.receiver_id == user.id) 
              and conversation.item_id == item.id) 
            |> select([item, user, conversation], 
              %{  "item": item,  
                  "user": user, 
                  "conversationId": conversation.id,
                  "distance": nil
                })
            |> order_by([item,_], [desc: item.updated_at]) 
            |> Repo.paginate(page: page) 
    end  
    render(conn, "index.json", item: item.entries, page_number: item.page_number,
    page_size: item.page_size,
    total_pages: item.total_pages,
    total_entries: item.total_entries)
  end



  def create(conn, %{"item" => item_params}) do
    changeset = Item.changeset(%Item{}, item_params)

    case Repo.insert(changeset) do
      {:ok, item} ->
        conn
        |> put_status(:created)
        |> put_resp_header("location", item_path(conn, :show, item))
        |> render("show.json", item: item)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Aerialroots.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    user = Guardian.Plug.current_resource(conn)
    Logger.info("m i here in show")
    item = Repo.get!(Item, id) |> Repo.preload([:itemtag, itemtag: :tag]) 
    render(conn, "show.json", item: item)
    image = "/vagrant/Berlin.jpg"
    getImage(conn, %{"id" => id, "image" => image})
  end

  def getImage(conn, %{"id" => id, "image" => image}) do
    Logger.info(image) 
  end

  def update(conn, %{"id" => id, "item" => item_params}) do
    user = Guardian.Plug.current_resource(conn)
    # image_preload_query = from i in Image, order_by: i.sequence
   # item =  Repo.get!(Item,id) |> preload(image: ^image_preload_query)
   item = Repo.get!(Item, id) |> Repo.preload([:itemtag, itemtag: :tag])  |> Repo.preload(:image)
    # |> Repo.preload(:image)
    changeset = Item.changeset(item, item_params)
    #IO.inspect(changeset)
    case Repo.update(changeset) do
      {:ok, item} ->
        render(conn, "showItem.json", item: item)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Aerialroots.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    #Bug  - item not deleted if conversation present
    #Fix - added conversation association to item model. added delete cascade to messages table(nested association)
    item = Item |> preload([:itemtag, itemtag: :tag]) |> preload(:image) |> preload([:conversation, conversation: :message]) |> Repo.get(id)
    #item = Repo.get!(Item, id) |> preload([:itemtag, itemtag: :tag, image: :image, conversation: :conversation])

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(item)

    send_resp(conn, :no_content, "")
  end

  def add_itemtag(conn, %{"item_id" => item_id, "itemtag" => itemtag_params}) do
    item = Item |> Repo.get(item_id) |> Repo.preload([:itemtag])  
    changeset = ItemTag.changeset(%ItemTag{}, Map.put(itemtag_params, "item_id", item_id))
    case Repo.insert(changeset) do
      {:ok, itemtag} ->
        IO.inspect(itemtag)
        conn
        |> put_status(:created)
        |> put_resp_header("location", item_tag_path(conn, :show, itemtag))
        |> render("itemtag.json", itemtag: itemtag)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Aerialroots.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def get_itemtag(conn, %{"item_id" => item_id}) do
    item = Item |> Repo.get(item_id) |> Repo.preload([:itemtag, itemtag: :tag]) 
    IO.inspect(item)
    render(conn, "item.json", item: item)
  end

   def unauthenticated(conn, _params) do
    conn
    |> put_flash(:error, "Authentication required")
    #|> redirect(to: login_path(conn, :create, :login))
    Logger.info("unauthenticated")
    send_resp(conn, :no_content ,"auth required")
  end

end
