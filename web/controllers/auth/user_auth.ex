 #auth token - refers to the provider token.It is not the header token
#                for identity provider, it refers to the user password  

#If supplied auth(email and provider combo) is found 
  # if provider is identity, get the password from the token field and match it with the provided  password
  # other providers, check the supplied auth credentials token with the auth token in database
# return auth 

  #if the auth token has expired
    # call replace_authorization (deletes existing provider token and creates a new one based on the supplied auth credentials)
    #  else user_from_authorization (gets the user from the valid authorization record)

#If auth is not found, its a new user, calls register_user_from_auth
    #creates user. creates auth. Stores the password or auth token in the token field based on the provider
    #

defmodule Aerialroots.UserFromAuth do
  #identified ?
  alias Aerialroots.User
  alias Aerialroots.Authorization
  alias Ueberauth.Auth
  require Logger
  def get_or_insert(auth, current_user, repo,anonymity) do
    case get_auth(auth, repo) do
      {:error, :not_found} -> register_user_from_auth(auth, current_user, repo, anonymity)
      {:error, reason} -> {:error, reason}
      authorization ->
        IO.inspect(authorization.expires_at)
        IO.inspect(Guardian.Utils.timestamp)
        if authorization.expires_at && authorization.expires_at < Guardian.Utils.timestamp do
           replace_authorization(authorization, auth, current_user, repo)
        else
          user_from_authorization(authorization, current_user, repo)
         end
    end
  end

  # We need to check the pw for the identity provider
  defp validate_auth_for_registration(%Auth{provider: :identity} = auth) do
    #validate password and password confirm fields for new reg
    Logger.info("validate both password fields from auth")
    if(is_nil(auth.info.email)) do
      {:error, :insufficient_info_email}
    else 
      if(is_nil(auth.info.name) && is_nil(auth.info.first_name) && is_nil(auth.info.last_name)) do
        {:error, :insufficient_info_name} 
      else  
        pw = Map.get(auth.credentials.other, :password)
        pwc = Map.get(auth.credentials.other, :password_confirmation)
        if pw == nil or pw == "" or pw == pwc, do: :ok, else: {:error, :password_confirmation_does_not_match}
      end        
    end     
  end

  # All the other providers are oauth so should be good
  defp validate_auth_for_registration(auth), do: :ok

  defp register_user_from_auth(auth, current_user, repo, anonymity) do
    case validate_auth_for_registration(auth) do
      :ok ->
        Logger.info("register_user_from_auth")
        case repo.transaction(fn -> create_user_from_auth(auth, current_user, repo, anonymity) end) do
          {:ok, response} -> response
          {:error, reason} -> {:error, reason}
        end
      {:error, reason} -> {:error, reason}
    end
  end

  defp replace_authorization(authorization, auth, current_user, repo) do
    # case validate_auth_for_registration(auth) do
    #   :ok ->
        case user_from_authorization(authorization, current_user, repo) do
          {:ok, user} ->
            case repo.transaction(fn ->
              repo.delete(authorization)
              authorization_from_auth(user, auth, repo)
              user
            end) do
              {:ok, user} -> {:ok, user}
              {:error, reason} -> {:error, reason}
            end
          {:error, reason} -> {:error, reason}
        end
      # {:error, reason} -> {:error, reason}
    # end
  end

  defp user_from_authorization(authorization, current_user, repo) do    
    case repo.one(Ecto.Model.assoc(authorization, :user)) do
      nil -> {:error, :user_not_found}
      user ->
        if current_user && current_user.id != user.id do
          {:error, :user_does_not_match}
        else
          {:ok, user}
        end
    end
  end

  defp create_user_from_auth(auth, current_user, repo, anonymity) do
    user = current_user
    #Logger.info(user)
    Logger.info("create user from auth")
    if !user, do: user = repo.get_by(User, email: auth.info.email)
    if !user, do: user = create_user(auth, repo, anonymity)
    authorization_from_auth(user, auth, repo)
    {:ok, user}      
  end

  defp create_user(auth, repo, anonymity) do
    name = name_from_auth(auth)
    result = User.changeset(%User{}, scrub(%{email: auth.info.email, name: name, anonymity: anonymity}))
    |> repo.insert
    case result do
      {:ok, user} -> user
      {:error, reason} -> repo.rollback(reason)
    end 
  end

  defp get_auth(%{provider: :identity} = auth, repo) do
    #if no authorization found for the email address provider combo, then do nothing
    IO.inspect(auth.credentials)
    case repo.get_by(Authorization, uid: uid_from_auth(auth), provider: to_string(auth.provider)) do
      nil -> {:error, :not_found}
      authorization ->
        if(is_nil(auth.credentials.other.password_confirmation)) do
          Logger.info("auth found")  
            case auth.credentials.other.password do
              pass when is_binary(pass) ->
                Logger.info("decrypting password")
                if Comeonin.Bcrypt.checkpw(auth.credentials.other.password, authorization.token) do
                  authorization
                else
                  {:error, :password_does_not_match}
                end
              _ -> {:error, :password_required}
            end
        else  
          if(authorization.active == true) do
           {:error, :user_already_registered}  
          else  
            {:error, :user_already_registered_not_active}  
          end    
        end  
        # #user came in from registration screen
        # if(is_nil(auth.credentials.other.password_confirmation)) do
        #   IO.inspect(auth.credentials.other.password_confirmation)
        # else  
        #   {:error, :user_already_registered}  
        # end
    end
  end

  defp get_auth(auth, repo) do
    Logger.info("Token provided by fb #{auth.credentials.token}" )
    case repo.get_by(Authorization, uid: uid_from_auth(auth), provider: to_string(auth.provider)) do
      nil -> {:error, :not_found}
      authorization ->
        authorization
        # if authorization.token == auth.credentials.token do
        #   authorization
        # else          
        #   {:error, :token_mismatch}
        # end
    end
  end

  defp authorization_from_auth(user, auth, repo) do
    Logger.info("build authorization from auth")
    authorization = Ecto.Model.build(user, :authorizations)
    result = Authorization.changeset(
      authorization,
      scrub(
        %{
          provider: to_string(auth.provider),
          uid: uid_from_auth(auth),
           token: token_from_auth(auth),
           refresh_token: auth.credentials.refresh_token,
           expires_at: auth.credentials.expires_at,
          password: password_from_auth(auth),
          password_confirmation: password_confirmation_from_auth(auth)
        }
      )
    )
    IO.inspect(result)
    insert = result |> repo.insert

    case insert do
      {:ok, the_auth} -> the_auth
      {:error, reason} -> repo.rollback(reason)
    end
  end

  defp name_from_auth(auth) do
      if auth.info.name do
        auth.info.name
      else
        [auth.info.first_name, auth.info.last_name]
        |> Enum.filter(&(&1 != nil and String.strip(&1) != ""))
        |> Enum.join(" ")
      end      
  end

  defp token_from_auth(%{provider: :identity} = auth) do
    Logger.info("hashing password")
    case auth do
      %{ credentials: %{ other: %{ password: pass } } } when not is_nil(pass) ->
        Comeonin.Bcrypt.hashpwsalt(pass)
      _ -> nil
    end
  end

  defp token_from_auth(auth), do: auth.credentials.token

  defp uid_from_auth(%{ provider: :slack } = auth), do: auth.credentials.other.user_id
  defp uid_from_auth(auth), do: auth.uid

  defp password_from_auth(%{provider: :identity} = auth), do: auth.credentials.other.password
  defp password_from_auth(_), do: nil

  defp password_confirmation_from_auth(%{provider: :identity} = auth) do
    auth.credentials.other.password_confirmation
  end
  defp password_confirmation_from_auth(_), do: nil

  # We don't have any nested structures in our params that we are using scrub with so this is a very simple scrub
  defp scrub(params) do
    result = Enum.filter(params, fn
      {key, val} when is_binary(val) -> String.strip(val) != ""
      {key, val} when is_nil(val) -> false
      _ -> true
    end)
    |> Enum.into(%{})
    result
  end
end
