defmodule Aerialroots.GuardianSerializer do
  @behaviour Guardian.Serializer

  alias Aerialroots.Repo
  alias Aerialroots.User
    alias Aerialroots.Authorization

  def for_token(user = %User{}), do: { :ok, "User:#{user.id}" }
  def for_token(_), do: { :error, "Unknown resource type" }

  def from_token("User:" <> id), do: { :ok, Repo.get(User, id) }
  def from_token(_), do: { :error, "Unknown resource type" }


  # def for_token(authorization = %Authorization{}), do: { :ok, "Uid:#{authorization.uid}" }
  # def for_token(_), do: { :error, "Unknown resource type" }

  # def from_token("Uid:" <> uid), do: { :ok, Repo.get(Authorization, uid) }
  # def from_token(_), do: { :error, "Unknown resource type" }
end
