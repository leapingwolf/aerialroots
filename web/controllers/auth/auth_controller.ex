defmodule Aerialroots.AuthController do
  use Aerialroots.Web, :controller
  require Logger
  alias Aerialroots.User
  alias Aerialroots.UserQuery
  alias Aerialroots.UserFromAuth
  alias Aerialroots.Authorization
  plug Ueberauth
  alias Aerialroots.Mailer
  alias Ueberauth.Strategy.Helpers
  #compile
def loadCurrentResource(conn, _params) do
  current_user = Guardian.Plug.current_resource(conn)
  case current_user do
    nil -> 
      send_resp(conn, 500 , "no current resource")
      IO.inspect("current timestamp is #{Guardian.Utils.timestamp}")
    %User{} -> 
      IO.inspect(Guardian.Plug.current_token(conn))
      IO.inspect(current_user)
      send_resp(conn, 200, "resource found")
    _ ->  send_resp(conn, 500, "error")
  end
end


  def request(conn, _params) do    
    IO.inspect(Helpers.callback_url(conn))
    render(conn, "request.html", callback_url: Helpers.callback_url(conn))
  end
  
#   def login(conn, current_user) do
#     Logger.info("in login")
#     send_resp(conn, 200 , "done")     
# #    current_user = Guardian.Plug.current_resource(conn)
#  #   IO.inspect(current_user)
#     #Logger.info("rendering login page")
#     #render conn, "login.html", current_user: current_user, current_auths: auths(current_user)
#   end


def callback(conn, %{"test" => params}) do  
  a = 1
  case a do
    0 -> send_resp(conn, 500 , "never here")     
    1 -> send_resp(conn, 300 , "never here")     
  end
end

def callback(conn, %{"user" => user_params}) do  
  # IO.inspect(conn)
  anonymity=false
  if user_params["anonymity"] do anonymity = user_params["anonymity"] end
  auth = conn.assigns.ueberauth_auth
  # IO.inspect(auth.token)
  current_user = Guardian.Plug.current_resource(conn)
  return = UserFromAuth.get_or_insert(auth, current_user, Repo, anonymity)
  case return  do
    {:ok, user} ->  
        #if provider is identity and status if false, do not sign in
        case request_activation(auth) do
          {:ok} ->  sign_in(conn,user,auth)   
          {:error, reason} ->    
            conn
           |> put_resp_header("info", "user reg or login error")
           |> render(Aerialroots.AuthView, "response.json", registrationresponse: %{"error" => reason})
          _ ->   
            Logger.info("never here")
            send_resp(conn, 500 , "never here")     
        end
    {:error, reason} ->
       conn
       |> put_resp_header("info", "user reg or login error")
       |> render(Aerialroots.AuthView, "response.json", registrationresponse: %{"error" => reason})
    _ ->   
      send_resp(conn, 500 , "never here")          
  end
     # send_resp(conn, 500 , "never here")          
end  

  # def callback(conn,params) do
  #   auth = conn.assigns.ueberauth_auth
  #   current_user = Guardian.Plug.current_resource(conn)
  #   #auth = auths(current_user)
  #   IO.inspect("current user : #{current_user}")
  #   IO.inspect(auth.uid)
  #   send_resp(conn, 200, "no content")        
  # end

  def callback(conn, params) do
   # IO.inspect(conn)
    auth = conn.assigns.ueberauth_auth
    IO.inspect(auth)
    current_user = Guardian.Plug.current_resource(conn)
    # #auth = auths(current_user)
    return = UserFromAuth.get_or_insert(auth, current_user, Repo, false)
    IO.inspect(return)
    case return  do
      {:ok, user} ->  sign_in(conn,user,auth)   
      {:error, reason} ->
         conn
         |> put_resp_header("info", "user reg or login error")
         |> render(Aerialroots.AuthView, "response.json", registrationresponse: %{"error" => reason})
      _ ->   
        send_resp(conn, 500 , "never here")          
    end
  end


  def logout(conn, _params) do   
    jwt = Guardian.Plug.current_token(conn)
    IO.inspect("current token is #{jwt}")
    if jwt do
      {:ok, claims}  = Guardian.Plug.claims(conn)
      Guardian.revoke!(jwt, claims)
    end    
    send_resp(conn, 200, "logged out successfully")
  end 
  

  defp sign_in(conn, user, %{provider: :identity} = auth) do
    authorization = Repo.one(UserQuery.auth_by_uid(auth.uid, %{provider: :identity} = auth))
    if authorization.active do
      Logger.info("can i sign in")
      create_api_session(conn,user)
    else
      conn
     |> put_resp_header("info", "inactive user")
     |> render(Aerialroots.AuthView, "response.json", registrationresponse: %{"user" => user, "error" => :inactive_user})
    end 
  end  

  defp sign_in(conn,user,auth) do create_api_session(conn,user) end  

  defp create_api_session(conn,user) do
     Logger.info("signing in")
     new_conn = Guardian.Plug.api_sign_in(conn, user)
     jwt = Guardian.Plug.current_token(new_conn)
     case Guardian.Plug.claims(new_conn) do
      {:ok, claims} -> Guardian.Plug.claims(new_conn)
        exp = Map.get(claims, "exp")     
        new_conn
         |> put_resp_header("authorization", "Bearer #{jwt}")
         |> render(Aerialroots.AuthView, "response.json", registrationresponse: %{"user" => user, "error" => "", "authorization" => "Bearer #{jwt}"} ) 
      _ ->    
        conn
       |> put_resp_header("info", "session creation failed")
       |> render(Aerialroots.AuthView, "response.json", registrationresponse: %{"error" => :session_failure})
      end 
  end   


  defp request_activation(%{provider: :identity} = auth) do    
    authorization = Repo.one(UserQuery.auth_by_uid(auth.uid, %{provider: :identity} = auth))
    if(authorization.active == false) do
       Logger.info("send email")      
       case Mailer.send_activation_email(authorization.uid, authorization.token) do
         {:error, reason}  -> {:error, reason}
         _ -> {:ok}
       end
     else
      {:ok}
     end
  end             

  #defp request_activation(%{provider: :identity} = auth, true, email) do {:ok} end

  defp  request_activation(auth) do {:ok} end

  #ex:http://localhost:4000/auth/activate?token=$2b$12$TQ03vSOPxf5lqgPwZN3Qxug/xwYEkzvP/UeBecbt3S.pqVugdyY7S&email=newuser@b.com
    def activate(conn, params) do
    Logger.info("in activation")
    email = params["email"]
    token = params["token"]
    IO.inspect("#{email} and #{token}")
      authorization = Repo.one(UserQuery.auth_by_uid(email, %{provider: :identity}))
      case authorization do
        nil -> Logger.info("auth does not exist")
        authorization ->
           if  authorization.token == token do
            Logger.info("set status to true")
            changeset = Authorization.changeset(authorization, %{active: true})
            Repo.update!(changeset)
            conn 
            |> put_layout(false) 
            |> render("activated.html")
           else 
            Logger.info("token mismatch")
            conn 
            |> put_layout(false) 
            |> render("error.html", %{error: "token mismatch"})
            
           end 
        {:error, reason} -> 
          Logger.info("invalid activation link")
            conn 
            |> put_layout(false) 
            |> render("error.html", %{error: "invalid activation link"})
      end
    send_resp(conn, :no_content, "user activation")
  end

  defp auths(nil), do: []
  defp auths(%Aerialroots.User{} = user) do
    Ecto.Model.assoc(user, :authorizations)
      |> Repo.all
      |> Enum.map(&(&1.provider))
  end 

end