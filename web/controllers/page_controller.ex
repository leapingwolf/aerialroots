defmodule Aerialroots.PageController do
  use Aerialroots.Web, :controller
  require Logger
  import Ecto.Query
  alias Aerialroots.Item
  alias Aerialroots.Image
  alias Aerialroots.ImageController

  def index(conn, _params) do
    conn
    |> put_layout(false)
    |> render("index3.html")
  end

  def testGetImageRecords(conn, %{"id" => id}) do
    item = Repo.get!(Item, id) |> Repo.preload([:image]) 
    conn |> render(Aerialroots.ItemView, "image.json", item: item)
  end  

  def insertImageRecord(conn, %{"id" => id}) do
    item = Repo.get!(Item, id) |> Repo.preload([:image]) 
    conn |> render(Aerialroots.ItemView, "image.json", item: item)
  end  

 def uploadImage(conn, %{"picture" => imgRawdata, "image" => image_params}) do 
	IO.inspect("M In upload image")
    parsedImageParams = Poison.Parser.parse!(image_params)
    # parsedImageId = Poison.Parser.parse!(id)
    IO.inspect(parsedImageParams)
    current_item = Map.put(parsedImageParams["image"], "id", 192) 
                  # |> Map.put("image_id", parsedImageId["id"])
                  # |> Map.put(parsedImageParams["image"], "filename", filename)
    task = Task.async(Image, :store, [{imgRawdata.path, current_item}])
    #return = Image.store({imgRawdata.path, current_item})
    return =  Task.await(task)

    # Logger.info("image upload return")
    case return do
       {:ok, path} ->
       	  sequence = Map.get(parsedImageParams["image"], "sequence")
          url = Image.url({imgRawdata.filename, current_item})
          image_params = Map.put(parsedImageParams["image"], "url", url)
          image_params = Map.put(image_params, "item_id", "192")
          image_params = Map.put(image_params, "uploadStatus", true)
          # IO.inspect(image_params)
		  #if image for the item id and sequence exists, replace it
		  existingImageRecord = Image 
		  					|> where([image], image.sequence == ^sequence and image.item_id == 192)
		  					|> Aerialroots.Repo.all()

		   case Enum.count(existingImageRecord) do
		   		0 -> 
		   		ImageController.create(conn, %{"image" => image_params})	
				# changeset = Image.changeset(%Image{}, image_params)	
			 #    case Repo.insert(changeset) do
			 #      {:ok, image} ->
			 #        conn
			 #        |> put_status(:created)
			 #        |> put_resp_header("location", image_path(conn, :show, image))
			 #        |> render(Aerialroots.ImageView, "show.json", image: image)
			 #      {:error, changeset} ->
			 #        conn
			 #        |> put_status(:unprocessable_entity)
			 #        |> render(Aerialroots.ChangesetView, "error.json", changeset: changeset)
			 #    end
				1 ->  
					imageId = Enum.at(existingImageRecord,0).id
					# image_params = Map.put(image_params, "id", imageId)	
					ImageController.update(conn, %{"id" => imageId, "image" => image_params}) 
				# 	image = Repo.get!(Image, imageId)
    # 				changeset = Image.changeset(image, image_params)
				# case Repo.update(changeset) do
			 #      {:ok, image} ->
			 #        conn
			 #        |> put_status(:created)
			 #        |> put_resp_header("location", image_path(conn, :show, image))
			 #        |> render(Aerialroots.ImageView, "show.json", image: image)
			 #      {:error, changeset} ->
			 #        conn
			 #        |> put_status(:unprocessable_entity)
			 #        |> render(Aerialroots.ChangesetView, "error.json", changeset: changeset)
			 #    end  
			end

      {:error, :invalid_file} ->         
        conn
        |> put_status(:error)
        |> render(Aerialroots.ChangesetView, "error.json", upload: :error)
      {:error, :timeout} ->  
      	IO.inspect("here") 
      	send_resp(conn, :timeout, "connection time out")
      {:error, error} -> 
      IO.inspect("here11")       	
		conn
        |> put_status(:error)
        |> render(Aerialroots.ChangesetView, "error.json", upload: error)
    end    
end

end