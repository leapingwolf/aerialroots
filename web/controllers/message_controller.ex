defmodule Aerialroots.MessageController do
  use Aerialroots.Web, :controller
  require Logger
  alias Aerialroots.Message
  alias Aerialroots.Conversation
  alias Aerialroots.User
  alias Aerialroots.Item
  import Ecto.Query

  plug :scrub_params, "message" when action in [:create, :update]
  plug Guardian.Plug.EnsureAuthenticated, handler: __MODULE__

  # def index(conn, _params) do
  #   message = Repo.all(Message)
  #   render(conn, "index.json", message: message)
  # end

  # NEXT VERSION: get all messages for an item ordered by date irrespective of sender/receiver - similar to comments on an item
  def index(conn, %{"page" => page, "item_id" => item_id}) do
    message = Message 
              |> join(:left, [message], conversation in Conversation, message.conversation_id == conversation.id) 
              |> join(:left, [message], receiver in User,receiver.id == message.receiver_id) 
              |> join(:left, [message], sender in User,sender.id == message.sender_id) 
              |> join(:left, [message, conversation],  item in Item,item.id == conversation.item_id) 
              |> select([message,conversation,receiver,sender,item], %{ "message": message,  "receiver": receiver, "sender": sender, "item": item, "conversation": conversation})
              |> where([message,conversation,receiver,sender,item], (item.id == ^item_id))
              |> order_by([message], [message.inserted_at]) 
              |> Repo.paginate(page: page) 
    # |> where([item], item.id == ^item_id)
    render(conn, "showAllMessages.json", message: message.entries, page_number: message.page_number,
    page_size: message.page_size,
    total_pages: message.total_pages,
    total_entries: message.total_entries)
  end


  def create(conn, %{"message" => message_params}) do
    Logger.info("message create")
    changeset = Message.changeset(%Message{}, message_params)

    case Repo.insert(changeset) do
      {:ok, message} ->
        conn
        |> put_status(:created)
        #|> put_resp_header("location", message_path(conn, :show, message))
        |> render("show.json", message: message)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Aerialroots.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    message = Repo.get!(Message, id)
    render(conn, "show.json", message: message)
  end

  def update(conn, %{"id" => id, "message" => message_params}) do
    message = Repo.get!(Message, id)
    changeset = Message.changeset(message, message_params)

    case Repo.update(changeset) do
      {:ok, message} ->
        render(conn, "show.json", message: message)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Aerialroots.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    message = Repo.get!(Message, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(message)

    send_resp(conn, :no_content, "")
  end

  def unauthenticated(conn, _params) do
  conn
  |> put_flash(:error, "Authentication required")
  #|> redirect(to: login_path(conn, :create, :login))
  Logger.info("unauthenticated")
  send_resp(conn, :no_content ,"auth required")
  end
end
