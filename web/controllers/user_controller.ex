defmodule Aerialroots.UserController do
  use Aerialroots.Web, :controller
  require Logger
  alias Aerialroots.User
  alias Aerialroots.Image
  alias Aerialroots.Item
  import Ecto.Query

  plug Guardian.Plug.EnsureAuthenticated, handler: __MODULE__

  plug :scrub_params, "user" when action in [:create, :update]
  plug :scrub_params, "item" when action in [:add_item]

def test(conn) do
   send_resp(conn, 200, "done")
end

  def index(conn, _params) do
    user = Guardian.Plug.current_resource(conn)
    #user = Repo.all(User)
    render(conn, "index.json", user: user)
  end

  def create(conn, %{"user" => user_params}) do
    changeset = User.changeset(%User{}, user_params)

    case Repo.insert(changeset) do
      {:ok, user} ->
        conn
        |> put_status(:created)
        |> put_resp_header("location", user_path(conn, :show, user))
        |> render("show.json", user: user)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Aerialroots.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id, "page" => page, "orderBy" => orderBy}) do
    IO.inspect("in show")
    user = User
          |> join(:inner, [user], item in Item, user.id == item.user_id) 
          |> preload([:item, item: [:itemtag, itemtag: :tag], item: [:image]]) 
          |> where([user], user.id == ^id)
          |> order_by([item,_], [desc: item.inserted_at]) 
          |> Repo.paginate(page: page)

    IO.inspect(user.entries)
    render(conn, "index.json", user: user.entries,
          page_number: user.page_number,
          page_size: user.page_size,
          total_pages: user.total_pages,
          total_entries: user.total_entries)
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Repo.get!(User, id) |> Repo.preload(:item)
    changeset = User.changeset(user, user_params)

    case Repo.update(changeset) do
      {:ok, user} ->
        render(conn, "show.json", user: user)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Aerialroots.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Repo.get!(User, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(user)

    send_resp(conn, :no_content, "")
  end

  def add_item(conn, %{"user_id" => user_id, "item" => item_params}) do
  #user = User |> Repo.get(user_id) |> Repo.preload(:item)  
  # item_params["inserted_at"] == Ecto.Date.local()
  changeset = Item.changeset(%Item{}, Map.put(item_params, "user_id", user_id))
  # IO.inspect(item_params["inserted_at"])
    case Repo.insert(changeset) do
      {:ok, item} ->
        item = item |> Repo.preload([:itemtag, itemtag: :tag]) 
        # |> Repo.preload(:image)
             conn
            |> put_status(:created)
            |> put_resp_header("location", item_path(conn, :show, item))        
            |> render(Aerialroots.ItemView, "showItem.json", item: item)
      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> render(Aerialroots.ChangesetView, "error.json", changeset: changeset)
    end
  end

  def get_item(conn, %{"user_id" => user_id, "page" => page, "where" => where, "orderBy" => orderBy}) do   
    image_preload_query = from i in Image, order_by: i.sequence
   case orderBy do  
     "0" ->
      userItem = Item 
              |> join(:inner, [item], user in User, item.user_id == user.id) 
              |> preload([:itemtag, itemtag: :tag]) |> preload(image: ^image_preload_query)
              |> where([item,user], user.id == ^user_id and like(item.title, ^("%#{where}%")))
              |> select([item], %{ "item": item})
              |> order_by([item,_], [desc: item.inserted_at]) 
              |> Repo.paginate(page: page)
    "1" ->           
      userItem = Item 
        |> join(:inner, [item], user in User, item.user_id == user.id) 
        |> preload([:itemtag, itemtag: :tag]) |> preload(image: ^image_preload_query)
        |> where([item,user], user.id == ^user_id and like(item.title, ^("%#{where}%")))
        |> select([item], %{ "item": item})
        |> order_by([item,_], [desc: item.updated_at]) 
        |> Repo.paginate(page: page)
    
    _ ->
      userItem = Item 
        |> join(:inner, [item], user in User, item.user_id == user.id) 
        |> preload([:itemtag, itemtag: :tag]) |> preload(image: ^image_preload_query)
        |> where([item,user], user.id == ^user_id and like(item.title, ^("%#{where}%")))
        |> select([item], %{ "item": item})
        |> order_by([item,_], [desc: item.updated_at]) 
        |> Repo.paginate(page: page)
        
    end 
    render(conn, "userItemDetail2.json", userItem: userItem.entries,
          page_number: userItem.page_number,
          page_size: userItem.page_size,
          total_pages: userItem.total_pages,
          total_entries: userItem.total_entries)
  end

  def get_item(conn, %{"user_id" => user_id, "page" => page, "orderBy" => orderBy}) do
  image_preload_query = from i in Image, order_by: i.sequence
  case orderBy do
     "0" ->
        Logger.info("order_by inserted_at")    
        userItem = Item 
            |> join(:inner, [item], user in User, item.user_id == user.id) 
            |> preload([:itemtag, itemtag: :tag]) |> preload(image: ^image_preload_query)
            |> where([_,user], user.id == ^user_id)
            |> select([item], %{ "item": item})
            |> order_by([item,_], [desc: item.inserted_at]) 
            |> Repo.paginate(page: page)
    "1" ->  
        Logger.info("order_by updated_at")    
        userItem = Item 
          |> join(:inner, [item], user in User, item.user_id == user.id) 
          |> preload([:itemtag, itemtag: :tag]) |> preload(image: ^image_preload_query)
          |> where([_,user], user.id == ^user_id)
          |> select([item], %{ "item": item})
          |> order_by([item,_], [desc: item.updated_at]) 
          |> Repo.paginate(page: page)
    _ ->        
        Logger.info("order_by anything else")
        userItem = Item 
            |> join(:inner, [item], user in User, item.user_id == user.id) 
            |> preload([:itemtag, itemtag: :tag]) |> preload(image: ^image_preload_query)
            |> where([_,user], user.id == ^user_id)
            |> select([item], %{ "item": item})
            |> order_by([item,_], [desc: item.inserted_at]) 
            |> Repo.paginate(page: page)
    end      
    render(conn, "userItemDetail2.json", userItem: userItem.entries,
          page_number: userItem.page_number,
          page_size: userItem.page_size,
          total_pages: userItem.total_pages,
          total_entries: userItem.total_entries)
  end

 def unauthenticated(conn, _params) do
    conn
    |> put_flash(:error, "Authentication required")
    #|> redirect(to: login_path(conn, :create, :login))
    Logger.info("unauthenticated")
    send_resp(conn, :no_content ,"auth required")
  end
end
