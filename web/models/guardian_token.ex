defmodule Aerialroots.GuardianToken do
  use Aerialroots.Web, :model
  require Logger
  alias Aerialroots.Repo
  alias Aerialroots.GuardianSerializer

  @primary_key {:jti, :string, []}
  @derive {Phoenix.Param, key: :jti}
  schema "guardian_tokens" do
    field :aud, :string
    field :iss, :string
    field :sub, :string
    field :exp, :integer
    field :jwt, :string
    field :claims, :map

    timestamps
  end

  def for_user(user) do
    Logger.info("in for user")
    case GuardianSerializer.for_token(user) do
      {:ok, aud} ->
        (from t in Aerialroots.GuardianToken, where: t.sub == ^aud)
          |> Repo.all
      _ -> []
    end
  end
end
