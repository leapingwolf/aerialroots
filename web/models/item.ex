defmodule Aerialroots.Item do
  use Aerialroots.Web, :model

  schema "item" do
    field :title, :string
    field :description, :string
    field :available, :boolean, default: false
    field :reservationEndDate, :string
    field :latitude, :float
    field :longitude, :float
    field  :offerType, :integer
    timestamps
    belongs_to :user, Aerialroots.User, foreign_key: :user_id
    has_many :itemtag, Aerialroots.ItemTag, on_delete: :delete_all
    has_many :image, Aerialroots.Image, on_delete: :delete_all
    has_many :conversation, Aerialroots.Conversation, on_delete: :delete_all
    #has_many :tag, through: [:itemtag, :tag]
    #timestamps
  end

  @required_fields ~w(title available offerType)
  @optional_fields ~w(description reservationEndDate itemtag user_id inserted_at updated_at latitude longitude)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> cast_assoc(:itemtag, required: false)
    |> cast_assoc(:image, required: false)
  end
end
