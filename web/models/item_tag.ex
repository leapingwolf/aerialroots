defmodule Aerialroots.ItemTag do
  use Aerialroots.Web, :model

  schema "itemtag" do
    belongs_to :item, Aerialroots.Item, foreign_key: :item_id
    belongs_to :tag, Aerialroots.Tag, foreign_key: :tag_id 
    timestamps
  end

  @required_fields ~w()
  @optional_fields ~w(item_id tag_id)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end

  def changeset(model, params, :create) do
    # add validations for create.can be used for other models too.
    #call it like this: ItemTag.changeset(%ItemTag{}, itemtag, :create)
    IO.puts(params)
  end
end
