defmodule Aerialroots.Image do
  use Aerialroots.Web, :model
  use Arc.Definition


  require Logger

  schema "image" do
    field :uploadStatus, :boolean, default: false
    field :filename, :string
    field :type, :string
    field :fileNameWithlocalPath, :string
    field :sequence, :integer
    field :url, :string
    belongs_to :item, Aerialroots.Item, foreign_key: :item_id
    timestamps
  end

  @required_fields ~w(filename type sequence item_id)
  @optional_fields ~w(url fileNameWithlocalPath uploadStatus)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end

  @versions [:original, :thumb]
  @extension_whitelist ~w(.jpg .jpeg .gif .png .noextension)
  

  def acl(:thumb, _), do: :public_read

  def validate({file, _}) do   
    file_extension = file.file_name |> Path.extname |> String.downcase
    #its a hack cos our filenames are without extension
    if (file_extension == ""), do:  file_extension = ".noextension"     
    #file_extension = ".noextension"     
    Enum.member?(@extension_whitelist, file_extension)
  end

  def transform(:thumb, _) do
    {:convert, "-thumbnail 200x200^ -gravity center -extent 200x200 -format png", :png}
  end

  def filename(version,{file, current_item}) do
    # IO.inspect(current_item)
    #"#{current_item["image_id"]}_#{current_item["filename"]}" 
    "#{current_item["filename"]}" 
  end

  def filename(version, _) do
    version
  end

  def storage_dir(_, {file, item}) do
    "images/#{item["id"]}"
  end

  def default_url(:thumb) do
    "https://placehold.it/100x100"
  end
end

