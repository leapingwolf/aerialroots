defmodule Aerialroots.Message do
  use Aerialroots.Web, :model

  schema "message" do
    belongs_to :conversation, Aerialroots.Conversation, foreign_key: :conversation_id
    field :sequence, :integer
    field :body, :string
    field :sender_id, :integer
    field :receiver_id, :integer

    timestamps
  end

  @required_fields ~w(conversation_id)
  @optional_fields ~w(sequence body sender_id receiver_id)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
  end
end
