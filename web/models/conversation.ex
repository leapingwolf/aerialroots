defmodule Aerialroots.Conversation do
  use Aerialroots.Web, :model
  require Logger

  schema "conversation" do
    belongs_to :item, Aerialroots.Item, foreign_key: :item_id
    belongs_to :sender, Aerialroots.User, foreign_key: :sender_id
    belongs_to :receiver, Aerialroots.User, foreign_key: :receiver_id
    has_many :message, Aerialroots.Message, on_delete: :delete_all
    timestamps
  end

  @required_fields ~w(item_id sender_id receiver_id)
  @optional_fields ~w()

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    Logger.info("m i here in :empty")
    model
    |> cast(params, @required_fields, @optional_fields)
    |> cast_assoc(:message, required: true)
  end

  def changeset(model, params, :upsert) do
    IO.inspect(params)
    # make sure only dates can be updated
     model
    |> cast(params, @required_fields, @optional_fields)
    #call it like this: ItemTag.changeset(%ItemTag{}, itemtag, :create)
    #IO.inspect(updated_at)
  end

end
