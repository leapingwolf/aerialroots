defmodule Aerialroots.User do
  use Aerialroots.Web, :model

#need to take care of anonymity,firstname and lastname fields
  schema "user" do
    field :email, :string
    field :name, :string
    # field :active, :boolean, default: false
    field :anonymity, :boolean, default: false
    has_many :authorizations, Aerialroots.Authorization
    has_many :item, Aerialroots.Item, on_delete: :delete_all
    timestamps
  end

  @required_fields ~w(name email)
  @optional_fields ~w(anonymity)

  # def registration_changeset(model, params \\ :empty) do
  #   model
  #   |>cast(params, ~w(email name), ~w())
  # end

  # def changeset(model, params \\ :empty) do
  #   model
  #   |> cast(params, @required_fields, @optional_fields)
  # end

  def login_changeset(model), do: model |> cast(%{}, ~w(), ~w(email))

  def login_changeset(model, params) do
    model
    |> cast(params, ~w(email), ~w())
    #|> validate_password
  end

  def validate_password(credentials) do
    #implement here
  end

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    model
    |> cast(params, @required_fields, @optional_fields)
    |> cast_assoc(:item, required: true)
  end
end
