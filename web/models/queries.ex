defmodule Aerialroots.UserQuery do
  import Ecto.Query
  alias Aerialroots.User
  alias Aerialroots.Authorization

  def user_by_email(email) do
    #from u in User, where: u.email == ^email
    User |> where([u], u.email == ^email and u.active == false)
  end

 def user_by_id(id) do
    User |> where([u], u.id == ^id)
  end

  def auth_by_uid(email, %{provider: :identity}) do
  	Authorization |> where([a], a.uid == ^email)
  end

  def auth_by_uid(email, %{provider: :facebook}) do
    Authorization |> where([a], a.uid == ^email)
  end
end