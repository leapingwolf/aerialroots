defmodule Aerialroots.Authorization do
  use Aerialroots.Web, :model
  require Logger
  schema "authorizations" do
    field :provider, :string
    field :uid, :string
    field :token, :string #hashed password while using identity. Token of the facebook or google api when signing in with those 
    field :refresh_token, :string #not used currently
    field :expires_at, :integer  #null for identity. only other providers will provide this value
    field :password, :string, virtual: true
    field :password_confirmation, :string, virtual: true
    field :active, :boolean, default: false
    belongs_to :user, Aerialroots.User

    timestamps
  end

  @required_fields ~w(provider uid user_id token)
  @optional_fields ~w(refresh_token expires_at active)

  @doc """
  Creates a changeset based on the `model` and `params`.

  If no params are provided, an invalid changeset is returned
  with no validation performed.
  """
  def changeset(model, params \\ :empty) do
    #Logger.info("m i here in changeset")
    model
    |> cast(params, @required_fields, @optional_fields)
    |> foreign_key_constraint(:user_id)
    #|> unique_constraint(:provider_uid)
  end
end
