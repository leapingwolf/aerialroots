defmodule Aerialroots.TagView do
  use Aerialroots.Web, :view

  def render("index.json", %{tag: tag}) do
    %{data: render_many(tag, Aerialroots.TagView, "tag.json")}
  end

  def render("show.json", %{tag: tag}) do
    %{data: render_one(tag, Aerialroots.TagView, "tag.json")}
  end

  def render("tag.json", %{tag: tag}) do
    %{id: tag.id,
      description: tag.description}
  end
end
