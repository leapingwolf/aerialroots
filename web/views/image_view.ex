defmodule Aerialroots.ImageView do
  use Aerialroots.Web, :view

  def render("index.json", %{image: image}) do
    %{data: render_many(image, Aerialroots.ImageView, "image.json")}
  end

  def render("show.json", %{image: image}) do
    %{data: render_one(image, Aerialroots.ImageView, "image.json")}
  end

  def render("image.json", %{image: image}) do
    %{id: image.id,
      uploadStatus: image.uploadStatus,
      filename: image.filename,
      type: image.type,
      fileNameWithlocalPath: image.fileNameWithlocalPath,
      sequence: image.sequence,
      url: image.url}
  end
end
