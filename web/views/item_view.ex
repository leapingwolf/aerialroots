defmodule Aerialroots.ItemView do
  use Aerialroots.Web, :view
  require Logger

  def render("index.json", %{item: item}) do
      render_many(item, Aerialroots.UserView, "showItemDetail.json")
  end


  def render("show.json", %{item: item}) do
      render_one(item, Aerialroots.ItemView, "item.json")
  end

  def render("show.json", %{image: image}) do
      render_one(image, Aerialroots.ImageView, "image.json")
  end

  def render("showItem.json", %{item: item}) do
     render_one(item, Aerialroots.ItemView, "itemId.json")
  end

  def render("itemId.json", %{item: item}) do
    %{id: item.id,
      title: item.title,
      available: item.available}
  end    

  def render("image.json", %{item: item}) do
      %{id: item.id,
      image: render_many(item.image, Aerialroots.ImageView, "image.json")}
  end

  def render("item.json", %{item: item}) do
    %{id: item.id,
      title: item.title,
      description: item.description,
      available: item.available,
      reservationEndDate: item.reservationEndDate,
      latitude: item.latitude,
      longitude: item.longitude,
      inserted_at: item.inserted_at,
      updated_at: item.updated_at,
      offerType: item.offerType,
      image: render_many(item.image, Aerialroots.ImageView, "image.json"),
      itemtag: render_many(item.itemtag, Aerialroots.ItemTagView, "item_tag.json")}
  end

  def render("itemtag.json", %{itemtag: itemtag}) do
    %{itemtag: render_one(itemtag, Aerialroots.ItemTagView, "item_tag_insert.json")}
  end

end
