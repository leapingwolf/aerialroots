defmodule Aerialroots.AuthView do
  use Aerialroots.Web, :view

  # def render("show.json", %{user: user}) do
  #   %{id: user.id,
  #     name: user.name,
  #     email: user.email}
  # end

  def render("response.json", %{registrationresponse: value}) do
    # When encoded, the changeset returns its errors
    # as a JSON object. So we just pass it forward.
    %{error: value["error"],
      user: render_one(value["user"], Aerialroots.UserView, "user.json"),
      authorization: value["authorization"]}
  end

end