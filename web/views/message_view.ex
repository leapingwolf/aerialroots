defmodule Aerialroots.MessageView do
  use Aerialroots.Web, :view

  def render("index.json", %{message: message}) do
    %{data: render_many(message, Aerialroots.MessageView, "message.json")}
  end

  def render("showAllMessages.json", %{message: message}) do
    render_many(message, Aerialroots.MessageView, "messageAll.json")
  end

  def render("show.json", %{message: message}) do
    %{data: render_one(message, Aerialroots.MessageView, "message.json")}
  end

  def render("messageAll.json", %{message: message}) do
    %{
      sender_id: message.sender.id,
      senderName: message.sender.name,
      receiver_id: message.receiver.id,
      receiverName: message.receiver.name,
      itemId: message.item.id,
      itemOfferType: message.item.offerType,
      itemOwnerId: message.item.user_id,
      id: message.message.id,
      inserted_at: message.message.inserted_at
      }
  end


  def render("message.json", %{message: message}) do
    %{id: message.id,
      conversation_id: message.conversation_id,
      sequence: message.sequence,
      body: message.body,
      sender_id: message.sender_id,
      receiver_id: message.receiver_id,
      inserted_at: message.inserted_at}
  end

  def render("showDetail.json", %{message: message}) do
    %{ sender: message.sender,
       receiver: message.receiver,
       message: render_one(message.message, Aerialroots.MessageView, "message.json")}
  end

  def render("showAll.json", %{message: message}) do
    render_many(message, Aerialroots.MessageView, "showDetail.json")
  end


end
