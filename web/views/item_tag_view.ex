defmodule Aerialroots.ItemTagView do
  use Aerialroots.Web, :view
  require Logger
  def render("index.json", %{itemtag: itemtag}) do
    Logger.info("m i here 2")
    %{data: render_many(itemtag, Aerialroots.ItemTagView, "item_tag.json")}
  end

  def render("show.json", %{item_tag: item_tag}) do
    %{data: render_one(item_tag, Aerialroots.ItemTagView, "item_tag.json")}
  end

  def render("item_tag_insert.json", %{item_tag: item_tag}) do
    Logger.info("in item tag")
    %{id: item_tag.id,
      idItem: item_tag.item_id,
      idTag: item_tag.tag_id}
  end

  def render("item_tag.json", %{item_tag: item_tag}) do
    Logger.info("in item tag")
    %{id: item_tag.id,
      idItem: item_tag.item_id,
      idTag: item_tag.tag_id,
      tagDescription: render_one(item_tag.tag, Aerialroots.TagView, "tag.json")}
  end
end
