defmodule Aerialroots.UserView do
  require Logger
  use Aerialroots.Web, :view

  def render("index.json", %{user: user}) do
    %{data: render_many(user, Aerialroots.UserView, "userDetail.json")}
  end

  def render("show.json", %{user: user}) do
    %{user: render_one(user, Aerialroots.UserView, "userDetail.json")}
  end

  #send back item id for image upload
  def render("showItem.json", %{item: item}) do
    %{item: render_one(item, Aerialroots.ItemView, "itemId.json")}
  end

  def render("user.json", %{user: user}) do
    %{id: user.id,
      email: user.email,
      anonymity: user.anonymity,
      name: user.name}
  end

  def render("showItemDetail.json", %{user: userItem}) do
    %{user: render_one(userItem.user, Aerialroots.UserView, "user.json"),
      item: render_one(userItem.item, Aerialroots.ItemView, "item.json"),
      distance: userItem.distance,
      conversationId: userItem.conversationId}
  end

  # def render("showItemDetail1.json", %{user: userItem}) do
  #   %{user: render_one(userItem.user, Aerialroots.UserView, "user.json"),
  #     item: render_one(userItem.item, Aerialroots.ItemView, "item.json")}
  # end

  def render("showItemDetail2.json", %{user: userItem}) do
    render_one(userItem.item, Aerialroots.ItemView, "item.json")
  end


    def render("userDetail.json", %{user: user}) do
    %{id: user.id,
      name: user.name,
      email: user.email,
      anonymity: user.anonymity,
      item: render_many(user.item, Aerialroots.ItemView, "item.json")}
  end

  # def render("userItemDetail.json", %{userItem: userItem}) do
  #   render_many(userItem, Aerialroots.UserView, "showItemDetail1.json")
  # end

  def render("userItemDetail2.json", %{userItem: userItem}) do
    render_many(userItem, Aerialroots.UserView, "showItemDetail2.json")
  end


end
