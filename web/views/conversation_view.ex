defmodule Aerialroots.ConversationView do
  use Aerialroots.Web, :view

  def render("index.json", %{conversation: conversation}) do
    render_many(conversation, Aerialroots.ConversationView, "conversation.json")
  end

  def render("show.json", %{conversation: conversation}) do
      %{conversation: render_one(conversation, Aerialroots.ConversationView, "conversation.json")}
  end


  def render("showDetail.json", %{conversation: conversation}) do
      %{sender_id: conversation.sender.id,
      senderName: conversation.sender.name,
      receiver_id: conversation.receiver.id,
      receiverName: conversation.receiver.name,
      itemId: conversation.item.id,
      itemOfferType: conversation.item.offerType,
      itemOwnerId: conversation.item.user_id,
      itemTitle: conversation.item.title,
      itemAvailable: conversation.item.available,
      conversation: render_one(conversation.conversation, Aerialroots.ConversationView, "conversation.json"),
      image: render_one(conversation.image, Aerialroots.ImageView, "image.json")}
  end


  # def render("showDetail.json", %{conversation: conversation}) do
  #     %{conversationResponse: render_one(conversation, Aerialroots.ConversationView, "showResponse.json"),
  #     conversation: render_one(conversation.conversation, Aerialroots.ConversationView, "conversation.json"),
  #     image: render_one(conversation.image, Aerialroots.ImageView, "image.json")}
  # end

  def render("showResponse.json", %{conversation: conversation}) do
    %{receiverName: conversation.receiver.name,
      receiverEmail: conversation.receiver.email,
      itemTitle: conversation.item.title,
      itemAvailable: conversation.item.available
    }
  end  

  def render("conversation.json", %{conversation: conversation}) do
    %{id: conversation.id,  
      sender_id: conversation.sender_id,
      receiver_id: conversation.receiver_id,
      updated_at: conversation.updated_at,
      item_id: conversation.item_id,
      message: render_many(conversation.message, Aerialroots.MessageView, "message.json")}
  end


  def render("conversationUserDetail.json", %{conversationUserDetail: conversationUser}) do
    render_many(conversationUser, Aerialroots.ConversationView, "showDetail.json")
  end
  
end
