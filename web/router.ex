defmodule Aerialroots.Router do
  use Aerialroots.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    #plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :csrf do
    plug :protect_from_forgery
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :api_auth do  
    plug Guardian.Plug.VerifyHeader, realm: "Bearer"
    plug Guardian.Plug.LoadResource
    plug Plug.Parsers, parsers: [:urlencoded, :multipart, :json]
  end  

  scope "/", Aerialroots do
    pipe_through :browser # Use the default browser stack
    resources "/image", ImageController, except: [:new, :edit]
    get "/", PageController, :index
    delete "/itemtags", ItemTagController, :delete
    get "/tags", TagController, :index
  end

  scope "/auth", Aerialroots do
    pipe_through [:browser, :api_auth]
    get "/test", UserController, :test
    get "/activate", AuthController, :activate
    get "/resource", AuthController, :loadCurrentResource
    delete "/logout", AuthController, :logout
    get "/facebook", AuthController, :request
    get "/:identity/callback", AuthController, :callback
    post "/:identity/callback", AuthController, :callback    
    resources "/users", UserController do
      #routes to add items and get items for a user
      post "/items", UserController, :add_item
      get "/items", UserController,  :get_item
    end
    resources "/items", ItemController, except: [:new, :edit, :create] do
    #can get and post itemtags for an item
      post "/uploadImage", ImageController, :uploadImage
      post "/itemtags", ItemController, :add_itemtag
      get "/itemtags", ItemController,  :get_itemtag
    end
    resources "/conversations", ConversationController, except: [:new, :edit] do
        #json input with the reqd fields: {"message": {"body": "hello added through add_message", "composer_id": 4 }}
        post "/messages", ConversationController, :add_message 
        #get all messages for a conversation
        get  "/messages", ConversationController, :get_message
    end
    put "/conversations", ConversationController, :upsert
    resources "/messages", MessageController, except: [:new, :edit] do
    end  
  end

  # Other scopes may use custom stacks.
  # scope "/api", Aerialroots do
  #   pipe_through :api
  # end
end
