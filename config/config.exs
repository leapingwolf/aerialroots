# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Configures the endpoint
config :aerialroots, Aerialroots.Endpoint,
  url: [host: "pass-it-on.com.de"],
  root: Path.dirname(__DIR__),
  secret_key_base: "38fJVpj/4paHMymj5+p3iu+4rTKHPz8aENWHzUopmXM5EHWdKn5Gh67oxIccfZDM",
  render_errors: [accepts: ~w(html json)],
  pubsub: [name: Aerialroots.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]


config :logger, :error_log,
  path: "log/error.log",
  level: :error

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
import_config "config.secret.exs"

# Configure phoenix generators
config :phoenix, :generators,
  migration: true,
  binary_id: false


config :guardian, Guardian,
  allowed_algos: ["HS256"], # optional
  verify_module: Guardian.JWT,  # optional
  issuer: "Aerialroots",
  ttl: { 30, :days },
  verify_issuer: true, # optional
  secret_key: "mySecretKey",
  serializer: Aerialroots.GuardianSerializer,
  hooks: GuardianDb,
  permissions: %{
    default: [
      :read_profile,
      :write_profile,
      :read_token,
      :revoke_token,
    ]
  }

config :ueberauth, Ueberauth,
  providers: [
    facebook: { Ueberauth.Strategy.Facebook, [profile_fields: "name,email"]},
    # github: { Ueberauth.Strategy.Github, [] },
    google: { Ueberauth.Strategy.Google, [] },
    identity: { Ueberauth.Strategy.Identity, [
        callback_methods: ["POST"],
        #uid_field: :username,
        #nickname_field: :username,
        param_nesting: "user"
      ] },
    #slack: { Ueberauth.Strategy.Slack, [] }
  ]

  config :ueberauth, Ueberauth.Strategy.Facebook.OAuth,
  client_id: "187119841710283",
  client_secret: "9ee2a1605d09d7f73d01b95c4e06a470",
  redirect_uri: "http://pass-it-on.com.de/auth/facebook/callback"

#TODO: set up google auth
#config :ueberauth, Ueberauth.Strategy.Google.OAuth,
  #client_id: "542679141562-omj5i34n5uf8io08c2qqbu7i5l1spf5b.apps.googleusercontent.com",
  #client_secret: "580ap_N_1HjfcRLv0nvydIt0",
  #redirect_uri: "http://localhost:4000/auth/google/callback"

#TODO: set up guardian token ttl - see ueberauth example project
  config :guardian_db, GuardianDb,
       repo: Aerialroots.Repo