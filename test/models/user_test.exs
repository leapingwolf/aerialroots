defmodule Declutter.UserTest do
  use Declutter.ModelCase

  alias Declutter.User

  @valid_attrs %{anonymity: true, email: "some content", firstName: "some content", lastName: "some content", password: "some content", status: true}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = User.changeset(%User{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = User.changeset(%User{}, @invalid_attrs)
    refute changeset.valid?
  end
end
