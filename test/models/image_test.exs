defmodule Aerialroots.ImageTest do
  use Aerialroots.ModelCase

  alias Aerialroots.Image

  @valid_attrs %{fileNameWithlocalPath: "some content", filename: "some content", sequence: 42, type: "some content", uploadStatus: true, url: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Image.changeset(%Image{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Image.changeset(%Image{}, @invalid_attrs)
    refute changeset.valid?
  end
end
