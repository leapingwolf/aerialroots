defmodule Declutter.ItemTagTest do
  use Declutter.ModelCase

  alias Declutter.ItemTag

  @valid_attrs %{idItem: 42, idTag: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = ItemTag.changeset(%ItemTag{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = ItemTag.changeset(%ItemTag{}, @invalid_attrs)
    refute changeset.valid?
  end
end
