defmodule Declutter.ConversationTest do
  use Declutter.ModelCase

  alias Declutter.Conversation

  @valid_attrs %{idItem: 42, idReceiver: 42, idSender: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Conversation.changeset(%Conversation{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Conversation.changeset(%Conversation{}, @invalid_attrs)
    refute changeset.valid?
  end
end
