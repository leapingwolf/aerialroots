defmodule Declutter.ItemTest do
  use Declutter.ModelCase

  alias Declutter.Item

  @valid_attrs %{available: true, description: "some content", reservationEndDate: "2010-04-17 14:00:00", title: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Item.changeset(%Item{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Item.changeset(%Item{}, @invalid_attrs)
    refute changeset.valid?
  end
end
