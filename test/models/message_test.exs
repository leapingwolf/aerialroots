defmodule Declutter.MessageTest do
  use Declutter.ModelCase

  alias Declutter.Message

  @valid_attrs %{body: "some content", composer_id: 42, conversation_id: 42, sequence: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Message.changeset(%Message{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Message.changeset(%Message{}, @invalid_attrs)
    refute changeset.valid?
  end
end
