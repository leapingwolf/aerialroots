defmodule Declutter.ConversationControllerTest do
  use Declutter.ConnCase

  alias Declutter.Conversation
  @valid_attrs %{idItem: 42, idReceiver: 42, idSender: 42}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, conversation_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "shows chosen resource", %{conn: conn} do
    conversation = Repo.insert! %Conversation{}
    conn = get conn, conversation_path(conn, :show, conversation)
    assert json_response(conn, 200)["data"] == %{"id" => conversation.id,
      "idSender" => conversation.idSender,
      "idReceiver" => conversation.idReceiver,
      "idItem" => conversation.idItem}
  end

  test "does not show resource and instead throw error when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, conversation_path(conn, :show, -1)
    end
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, conversation_path(conn, :create), conversation: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(Conversation, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, conversation_path(conn, :create), conversation: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    conversation = Repo.insert! %Conversation{}
    conn = put conn, conversation_path(conn, :update, conversation), conversation: @valid_attrs
    assert json_response(conn, 200)["data"]["id"]
    assert Repo.get_by(Conversation, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    conversation = Repo.insert! %Conversation{}
    conn = put conn, conversation_path(conn, :update, conversation), conversation: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    conversation = Repo.insert! %Conversation{}
    conn = delete conn, conversation_path(conn, :delete, conversation)
    assert response(conn, 204)
    refute Repo.get(Conversation, conversation.id)
  end
end
