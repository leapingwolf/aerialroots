defmodule Declutter.ItemTagControllerTest do
  use Declutter.ConnCase

  alias Declutter.ItemTag
  @valid_attrs %{idItem: 42, idTag: 42}
  @invalid_attrs %{}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, item_tag_path(conn, :index)
    assert json_response(conn, 200)["data"] == []
  end

  test "shows chosen resource", %{conn: conn} do
    item_tag = Repo.insert! %ItemTag{}
    conn = get conn, item_tag_path(conn, :show, item_tag)
    assert json_response(conn, 200)["data"] == %{"id" => item_tag.id,
      "idItem" => item_tag.idItem,
      "idTag" => item_tag.idTag}
  end

  test "does not show resource and instead throw error when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, item_tag_path(conn, :show, -1)
    end
  end

  test "creates and renders resource when data is valid", %{conn: conn} do
    conn = post conn, item_tag_path(conn, :create), item_tag: @valid_attrs
    assert json_response(conn, 201)["data"]["id"]
    assert Repo.get_by(ItemTag, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, item_tag_path(conn, :create), item_tag: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "updates and renders chosen resource when data is valid", %{conn: conn} do
    item_tag = Repo.insert! %ItemTag{}
    conn = put conn, item_tag_path(conn, :update, item_tag), item_tag: @valid_attrs
    assert json_response(conn, 200)["data"]["id"]
    assert Repo.get_by(ItemTag, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    item_tag = Repo.insert! %ItemTag{}
    conn = put conn, item_tag_path(conn, :update, item_tag), item_tag: @invalid_attrs
    assert json_response(conn, 422)["errors"] != %{}
  end

  test "deletes chosen resource", %{conn: conn} do
    item_tag = Repo.insert! %ItemTag{}
    conn = delete conn, item_tag_path(conn, :delete, item_tag)
    assert response(conn, 204)
    refute Repo.get(ItemTag, item_tag.id)
  end
end
