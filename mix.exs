defmodule Aerialroots.Mixfile do
  use Mix.Project

  def project do
    [app: :aerialroots,
     version: "0.0.9",
     elixir: "~> 1.0",
     elixirc_paths: elixirc_paths(Mix.env),
     compilers: [:phoenix, :gettext] ++ Mix.compilers,
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     aliases: aliases,
     deps: deps]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [mod: {Aerialroots, []},
     applications: [:phoenix, :phoenix_html, :cowboy, :scrivener, :logger, :gettext, :guardian, :guardian_db,
                    :phoenix_ecto, :postgrex, :ueberauth_identity, :ueberauth_facebook, :ueberauth_google, :comeonin, :mailgun,
                    :ex_aws, :httpoison, :ssl, :conform, :arc]]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "web", "test/support"]
  defp elixirc_paths(_),     do: ["lib", "web"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [{:phoenix, "~> 1.1.1"},
     {:phoenix_ecto, "~> 2.0"},
     {:postgrex, ">= 0.0.0"},
     {:phoenix_html, "~> 2.3"},
     {:phoenix_live_reload, "~> 1.0", only: :dev},
     {:gettext, "~> 0.9"},
     {:cowboy, "~> 1.0"},
     {:scrivener, "~> 1.0"},
     {:exrm, "~> 0.19.9"},
     {:conform, "~> 0.17"},
     {:ueberauth_identity, "~>0.2.1"},
     {:ueberauth_facebook, "~> 0.1"},
     {:ueberauth_google, "~> 0.1"},
     {:guardian, "0.9.0"},
     {:guardian_db, "0.4.0"},
     {:comeonin, "~> 1.6"},
     {:mailgun, "~> 0.1.2"}, 
     {:arc, "~> 0.5.2"},
     {:ex_aws, "~> 0.4.10"}, # Required if using Amazon S3
     {:httpoison, "~> 0.7"},  # Required if using Amazon S3
     {:poison, "~> 1.2"}]     # Required if using Amazon S3]
     # {:erlcloud, "~> 0.13.3"}]
  end

  # Aliases are shortcut or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    ["ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
     "ecto.reset": ["ecto.drop", "ecto.setup"]]
  end
end
