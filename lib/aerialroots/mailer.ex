defmodule Aerialroots.Mailer do
  use Mailgun.Client,
      domain: Application.get_env(:aerialroots, :mailgun_domain),
      key: Application.get_env(:aerialroots, :mailgun_key) #,
      # mode: :test,
      # test_file_path: "/tmp/mailgun.json"
  #alias Junkauth.Router.Helpers
  #alias Junkauth.Endpoint

	# def send_welcome_text_email(user,confirmation_token) do
	# 	send_email to:  user.email,
 #           from: "service_desk@example.com",
 #           subject: "Welcome!",
 #           text: "Welcome to HelloPhJunkauth!",
 #           html: welcome_html
 #   end    

  def send_activation_email(email,confirmation_token) do
    path = Aerialroots.Router.Helpers.auth_path(Aerialroots.Endpoint, :activate)
    #IO.inspect(Router.Helpers.session_path(Endpoint, :activate), %{"confirmation_token" => confirmation_token})
    send_email to:  email,
           from: "noreply@pass-it-on.com.de",
           subject: "Activate your account!",
           html: Phoenix.View.render_to_string(Aerialroots.EmailView, "welcome.html", %{path: path, email: email, token: confirmation_token})

  end    

end