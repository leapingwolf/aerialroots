defmodule Aerialroots.Repo do
  use Ecto.Repo, otp_app: :aerialroots
  use Scrivener, page_size: 25, max_page_size: 100
end
