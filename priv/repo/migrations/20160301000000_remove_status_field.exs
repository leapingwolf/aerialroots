defmodule Aerialroots.Repo.Migrations.RemoveStatusField do
  use Ecto.Migration

  def change do
    alter table(:user) do
	  remove :status
    end
  end
end