defmodule Aerialroots.Repo.Migrations.AddIndexToUser do
  use Ecto.Migration

  def change do
  	create index(:user, [:email], unique: true)
  end
end
