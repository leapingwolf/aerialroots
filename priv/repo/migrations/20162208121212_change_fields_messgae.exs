defmodule Aerialroots.Repo.Migrations.ChangeFieldsMessage do
  use Ecto.Migration

  def change do
    alter table(:message) do
      remove :composer_id
      add :sender_id, references(:user) 
      add :receiver_id, references(:user) 
    end

  end
end
