defmodule Aerialroots.Repo.Migrations.CreateConversation do
  use Ecto.Migration

  def change do
    create table(:conversation) do
      add :item_id, references(:item)	
      add :sender_id, references(:user)
      add :receiver_id, references(:user)
      timestamps
    end

  end
end
