defmodule Aerialroots.Repo.Migrations.ChangeIndexMessage do
  use Ecto.Migration

  def change do
  	alter table(:message) do
  	  remove :conversation_id
      add :conversation_id, references(:conversation)
    end  
  end
end
