defmodule Aerialroots.Repo.Migrations.AddActiveFieldToAuthorizations do
  use Ecto.Migration

  def change do
    alter table(:authorizations) do 	
      add :active, :bool
    end
  end
 end
