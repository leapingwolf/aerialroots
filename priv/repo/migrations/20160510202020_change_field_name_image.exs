defmodule Aerialroots.Repo.Migrations.ChangeFieldNameImage do
  use Ecto.Migration

  def change do
    alter table(:image) do
      remove :idItem
      add :item_id, references(:item) 
    end

  end
end
