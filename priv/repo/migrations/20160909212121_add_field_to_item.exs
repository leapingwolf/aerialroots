defmodule Aerialroots.Repo.Migrations.AddTFieldToItem do
  use Ecto.Migration

  def change do
    alter table(:item) do
      add :offerType, :int
    end

  end
end
