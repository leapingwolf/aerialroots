defmodule Aerialroots.Repo.Migrations.ChangeFieldsItemTag do
  use Ecto.Migration

  def change do
  	alter table(:itemtag) do
  		remove :idItem
  		add :item_id, references(:item) 
  	end	
  end
end
