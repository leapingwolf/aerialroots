defmodule Aerialroots.Repo.Migrations.ChangeItem do
  use Ecto.Migration

  def change do
    alter table(:item) do
      modify :reservationEndDate, :string
    end
  end
end
