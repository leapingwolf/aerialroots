defmodule Aerialroots.Repo.Migrations.CreateTag do
  use Ecto.Migration

  def change do
    create table(:tag) do
      add :description, :string

      timestamps
    end

  end
end
