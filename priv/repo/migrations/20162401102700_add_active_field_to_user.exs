defmodule Aerialroots.Repo.Migrations.AddActiveFieldToUser do
  use Ecto.Migration

  def change do
    alter table(:user) do
	  add :name, :text    	
      add :active, :bool
    end
  end
end
