defmodule Aerialroots.Repo.Migrations.RemoveFieldsFromItemAgain do
  use Ecto.Migration

  def change do
  	 alter table(:item) do
  		remove :idUser
  		add :user_id, references(:user)
  	end	
  end
end
