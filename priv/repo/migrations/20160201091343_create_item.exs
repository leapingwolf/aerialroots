defmodule Aerialroots.Repo.Migrations.CreateItem do
  use Ecto.Migration

  def change do
    create table(:item) do
      add :title, :string
      add :description, :string
      add :available, :boolean, default: false
      add :reservationEndDate, :datetime
      #add :idUser, references(:user)
      #add :idItemtag, references(:itemtag)
      timestamps
    end

  end
end
