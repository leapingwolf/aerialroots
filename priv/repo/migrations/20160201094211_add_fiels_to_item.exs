defmodule Aerialroots.Repo.Migrations.AddFieldsToItem do
  use Ecto.Migration

  def change do
  	alter table(:item) do
  	  add :idUser, references(:user)
      add :idItemtag, references(:itemtag)
    end  
  end
end
