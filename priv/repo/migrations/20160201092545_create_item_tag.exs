defmodule Aerialroots.Repo.Migrations.CreateItemTag do
  use Ecto.Migration

  def change do
    create table(:itemtag) do
      add :idItem, references(:item) 
      add :idTag, references(:tag)

      timestamps
    end

  end
end
