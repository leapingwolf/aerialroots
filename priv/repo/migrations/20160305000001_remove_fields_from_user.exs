defmodule Aerialroots.Repo.Migrations.RemoveFieldsFromUser do
  use Ecto.Migration

  def change do
    alter table(:user) do
	  remove :firstName
	  remove :lastName
      remove :active
    end
  end
end
