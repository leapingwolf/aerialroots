defmodule Aerialroots.Repo.Migrations.AddFieldsToItem do
  use Ecto.Migration

  def change do
    alter table(:item) do
      add :longitude, :float
      add :latitude, :float
    end

  end
end
