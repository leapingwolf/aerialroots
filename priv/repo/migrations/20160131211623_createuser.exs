defmodule Aerialroots.Repo.Migrations.Createuser do
  use Ecto.Migration
#need to create an index on email address
  def change do
  	create table(:user) do 
  	    add :firstName, :string
	    add :lastName, :string
	    add :email, :string
	    add :password, :string
	    add :status, :boolean
	    add :anonymity, :boolean
	    timestamps
	 end
  end
end
