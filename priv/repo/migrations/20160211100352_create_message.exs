defmodule Aerialroots.Repo.Migrations.CreateMessage do
  use Ecto.Migration

  def change do
    create table(:message) do
      add :conversation_id, :integer
      add :sequence, :integer
      add :body, :text
      add :composer_id, :integer

      timestamps
    end

  end
end
