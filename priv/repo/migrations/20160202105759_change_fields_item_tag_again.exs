defmodule Aerialroots.Repo.Migrations.ChangeFieldsItemTagAgain do
  use Ecto.Migration

  def change do
  	 alter table(:itemtag) do
  		remove :idTag
  		add :tag_id, references(:tag) 
  	end	
  end
end
