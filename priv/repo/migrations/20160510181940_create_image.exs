defmodule Aerialroots.Repo.Migrations.CreateImage do
  use Ecto.Migration

  def change do
    create table(:image) do
      add :uploadStatus, :boolean, default: false
      add :filename, :string
      add :type, :string
      add :fileNameWithlocalPath, :string
      add :sequence, :integer
      add :url, :string
      add :idItem, references(:item)
      timestamps
    end

  end
end
